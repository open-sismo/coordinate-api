from rest_framework import serializers

from coordinate.apps.base.serializers import CatalogSerializer
from coordinate.apps.places.models import Place, PlaceType


class PlaceTypeSerializer(CatalogSerializer):
    class Meta(CatalogSerializer.Meta):
        model = PlaceType
        fields = '__all__'


class PlaceSerializer(CatalogSerializer):
    place_type = PlaceTypeSerializer(read_only=True)
    distance = serializers.FloatField(read_only=True, required=False)

    class Meta(CatalogSerializer.Meta):
        model = Place
        fields = '__all__'
        depth = 1

    def create(self, validated_data):
        if self.Meta.model.objects.filter(name=self.validated_data['name']).exists():
            raise serializers.ValidationError(
                "An {} with this name already exists".format(self.Meta.model.Meta.verbose_name))

        pt_data = validated_data.pop('place_type', {})
        pt_serializer = PlaceTypeSerializer(data=pt_data)

        if pt_serializer.is_valid():
            if PlaceType.objects.filter(name=pt_data['name']).exists():
                pt_serializer = PlaceTypeSerializer(
                    PlaceType.objects.filter(name=pt_data['name']).first())
            else:
                pt_serializer.save()
        coll_detail = self.Meta.model.objects.create(
            place_type=pt_serializer.instance, **validated_data)
        return coll_detail
