# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework import viewsets

from coordinate.apps.places.filters import PlaceFilter, PlaceTypeFilter
from coordinate.apps.places.models import Place, PlaceType
from coordinate.apps.places.serializers import PlaceSerializer, PlaceTypeSerializer


class PlaceViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Places to be viewed or edited.
    """
    queryset = Place.objects.all()
    serializer_class = PlaceSerializer
    filter_class = PlaceFilter
    lookup_field = 'slug'


class PlaceTypeViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Places to be viewed or edited.
    """
    queryset = PlaceType.objects.all()
    serializer_class = PlaceTypeSerializer
    filter_class = PlaceTypeFilter
    lookup_field = 'slug'
