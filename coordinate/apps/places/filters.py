# -*- coding: utf-8 -*-
import django_filters
from coordinate.apps.base.filters import CatalogFilter
from coordinate.apps.places.models import Place, PlaceType


class PlaceTypeFilter(CatalogFilter):
    class Meta(CatalogFilter.Meta):
        model = PlaceType



class PlaceTypeChoiceFilter(django_filters.ModelChoiceFilter):
    def get_filter_predicate(self, v):
        return {"place_type__slug": v.slug}


class PlaceFilter(CatalogFilter):
    place_type = PlaceTypeChoiceFilter(
        queryset=PlaceType.objects.all(), to_field_name="slug")
    near_to = django_filters.CharFilter(method='get_near_to', label="Near to")

    class Meta(CatalogFilter.Meta):
        model = Place
        fields = CatalogFilter.Meta.fields
        fields = {
            "place_type": ['exact'],
            "near_to": ["exact"]
        }
        fields.update(CatalogFilter.Meta.fields)

    def get_near_to(self, queryset, name, value):
        queryset = Place.objects.get_near_to(value, queryset=queryset)
        return queryset
