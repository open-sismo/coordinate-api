from import_export import resources
from coordinate.apps.places.models import Place


class PlaceResource(resources.ModelResource):

    class Meta:
        model = Place
        exclude = ("place_ptr", )
