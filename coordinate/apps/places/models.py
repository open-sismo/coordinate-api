# -*- coding: utf-8 -*-
import re
from django.db import models
from geoposition.fields import GeopositionField

from coordinate.apps.base.models import Catalog, CatalogManager


class PlaceType(Catalog):
    class Meta(Catalog.Meta):
        verbose_name = "Place Type"
        verbose_name_plural = "Place Types"


class PlaceManager(CatalogManager):
    def get_near_to(self, point, queryset=None):
        if queryset is None:
            queryset = self.get_queryset()
        try:
            match = re.search(r'^(\-?\d+\.?\d*),(\-?\d+\.?\d*)$', point)
            lat, lng = match.groups()
        except:
            raise ValueError("Invalid coordinates.")

        queryset = queryset.extra(select={
            "distance": """ACOS(
                SIN(radians(cast(SPLIT_PART(position, ',', 1) as float))) *
                SIN(radians({lat})) +
                COS(radians(cast(SPLIT_PART(position, ',', 1) as float))) *
                COS(radians({lat})) *
                COS(radians({lng}) -radians(cast(SPLIT_PART(position, ',', 2) as float))) ) *
                6371000""".format(lat=lat, lng=lng)
        })
        queryset = queryset.order_by('distance')
        return queryset


class Place(Catalog):
    position = GeopositionField(verbose_name="Position")
    address = models.TextField(blank=True, verbose_name="Address")
    place_type = models.ForeignKey(
        PlaceType, null=True, related_name='place_type', verbose_name="Place Type")

    objects = PlaceManager()

    class Meta(Catalog.Meta):
        verbose_name = "Place"
        verbose_name_plural = "Places"
