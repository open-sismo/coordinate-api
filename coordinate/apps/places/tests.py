# -*- coding: utf-8 -*-
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from coordinate.apps.base.testcases import JWTAuthTestCaseMixin
from coordinate.apps.places.models import Place, PlaceType


class PlaceTests(JWTAuthTestCaseMixin, APITestCase):
    def setUp(self):
        self.obj = Place.objects.create(
            name="Springfield",
            position='19.5013887,-99.15964000000002'
        )
        super(PlaceTests, self).setUp()

    def jwt_auth(self, user):
        response = self.client.post(
            reverse('jwt-auth'),
            data=dict(username=self.user.username, password=self.password))
        token = response.data.get('token')
        self.assertIsNotNone(token)
        self.client.credentials(HTTP_AUTHORIZATION="JWT {}".format(token))
        return token

    def test_places_list(self):
        url = reverse('places-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_places_detail(self):
        url = reverse('places-detail', kwargs={'slug': self.obj.slug})
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_place(self):
        """
        Ensure we can create a new place object.
        """
        url = reverse('places-list')
        data = {
            'name': 'Test Place',
            'slug': 'test-place',
            'position': '19.5013887,-99.15964000000002',
            'address': 'Test Place #3, Roma Sur, CDMX'
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Place.objects.count(), 2)
        self.assertEqual(Place.objects.get(slug=response.data.get('slug')).name, 'Test Place')

    def test_update_place(self):
        """
        Ensure we can update a place object.
        """
        url = reverse('places-detail', kwargs={'slug': self.obj.slug})
        data = {
            'name': 'Shellbyville',
            'slug': 'shellbyville',
            'position': '19.5013887,-99.15964000000002',
            'address': 'Test Place #3, Roma Sur, CDMX'
        }
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Place.objects.count(), 1)
        self.assertEqual(Place.objects.get().name, 'Shellbyville')

    def test_delete_place(self):
        """
        Ensure we can delete a place object.
        """
        url = reverse('places-detail', kwargs={'slug': self.obj.slug})
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Place.objects.count(), 0)


class PlaceTypeTests(JWTAuthTestCaseMixin, APITestCase):
    def setUp(self):
        self.obj = PlaceType.objects.create(
            name="School"
        )
        super(PlaceTypeTests, self).setUp()

    def test_placetypes_list(self):
        url = reverse('placetypes-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_places_detail(self):
        url = reverse('placetypes-detail', kwargs={'slug': self.obj.slug})
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_place(self):
        """
        Ensure we can create a new place object.
        """
        url = reverse('placetypes-list')
        data = {
            'name': 'Test',
            'slug': 'test',
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(PlaceType.objects.count(), 2)
        self.assertEqual(PlaceType.objects.get(slug=response.data.get('slug')).name, 'Test')

    def test_update_place(self):
        """
        Ensure we can update a place object.
        """
        url = reverse('placetypes-detail', kwargs={'slug': self.obj.slug})
        data = {
            'name': 'Shellbyville',
            'slug': 'shellbyville',
        }
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(PlaceType.objects.count(), 1)
        self.assertEqual(PlaceType.objects.get().name, 'Shellbyville')

    def test_delete_place(self):
        """
        Ensure we can delete a place object.
        """
        url = reverse('placetypes-detail', kwargs={'slug': self.obj.slug})
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(PlaceType.objects.count(), 0)
