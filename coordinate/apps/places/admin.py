# -*- coding: utf-8 -*-
from django.contrib import admin
from coordinate.apps.base.admin import CatalogAdminMixin
from coordinate.apps.places.models import Place, PlaceType
from coordinate.apps.places.resources import PlaceResource


@admin.register(PlaceType)
class PlaceTypeAdmin(CatalogAdminMixin, admin.ModelAdmin):
    pass


@admin.register(Place)
class PlaceAdmin(CatalogAdminMixin, admin.ModelAdmin):
    resource_class = PlaceResource
    list_filter = CatalogAdminMixin.list_filter + ('place_type',)
