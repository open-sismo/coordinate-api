from import_export import resources
from coordinate.apps.flags.models import Flag


class FlagResource(resources.ModelResource):

    class Meta:
        model = Flag
