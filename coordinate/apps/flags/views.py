# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework import viewsets

# from coordinate.apps.places.filters import PlaceFilter, PlaceTypeFilter
from coordinate.apps.flags.models import Flag
from coordinate.apps.flags.serializers import FlagSerializer


class FlagViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Flags to be viewed or edited.
    """
    queryset = Flag.objects.all()
    serializer_class = FlagSerializer
    # filter_class = PlaceFilter
    lookup_field = 'slug'
