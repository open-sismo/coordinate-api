from rest_framework import serializers

from coordinate.apps.base.serializers import CatalogSerializer
from coordinate.apps.flags.models import Flag
from coordinate.apps.places.serializers import PlaceSerializer


class FlagSerializer(CatalogSerializer):
    places = PlaceSerializer(read_only=True, many=True)

    class Meta(CatalogSerializer.Meta):
        model = Flag
        fields = '__all__'
        # depth = 2
