# -*- coding: utf-8 -*-
from django.urls import reverse
import json
from rest_framework import status
from rest_framework.test import APITestCase
from coordinate.apps.base.testcases import JWTAuthTestCaseMixin
from coordinate.apps.flags.models import Flag
from coordinate.apps.places.models import Place

from coordinate.apps.flags.serializers import FlagSerializer

class FlagTests(JWTAuthTestCaseMixin, APITestCase):
    def setUp(self):
        self.obj1 = Place.objects.create(
            name="Springfield",
            position='19.5013887,-99.15964000000002'
        )

        self.obj2 = Flag.objects.create(
            name="Springfield needs water",
            slug="springfield-needs-water",
        )

        super(FlagTests, self).setUp()

    def jwt_auth(self, user):
        response = self.client.post(
            reverse('jwt-auth'),
            data=dict(username=self.user.username, password=self.password))
        token = response.data.get('token')
        self.assertIsNotNone(token)
        self.client.credentials(HTTP_AUTHORIZATION="JWT {}".format(token))
        return token

    def test_flags_list(self):
        url = reverse('flags-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_flag(self):
        """
        Ensure we can create a new flag object.
        """
        url1 = reverse('places-list')
        url2 = reverse('flags-list')

        auxGet = self.client.get(url1, format='json')
        res = json.loads(auxGet.content)

        data = {
            'name': 'Test Flag',
            'slug': 'test-flag',
            'places.id': res['results'][0]['id']
        }

        response = self.client.post(url2, data, format='json')

        
        # print (response)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Flag.objects.get(slug=response.data.get('slug')).name, 'Test Flag')

    def test_update_flag(self):
        """
        Ensure we can update a flag object.
        """
        url = reverse('flags-detail', kwargs={'slug': self.obj2.slug})


        data = {
            'name': 'Springfield needs nuclear plant!',
            'slug': self.obj2.slug
        }
        response = self.client.put(url, data, format='json')
        # print (response)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Flag.objects.count(), 1)
        self.assertEqual(Flag.objects.get().name, 'Springfield needs nuclear plant!')

    def test_delete_flag(self):
        """
        Ensure we can delete a flag object.
        """
        url = reverse('flags-detail', kwargs={'slug': self.obj2.slug})
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Flag.objects.count(), 0)


    def test_assign_flag_to_place(self):
        """
        Ensure we can assign a flag object to Place.
        """

        data = {
            'name': self.obj2.name,
            'slug': self.obj2.slug,
            'places.id': self.obj1.id
        }

        url = reverse('flags-detail', kwargs={'slug': self.obj2.slug})
        response = self.client.put(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsNotNone(Flag.objects.get().places)
