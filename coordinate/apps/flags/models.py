# -*- coding: utf-8 -*-
from django.db import models

from coordinate.apps.base.models import Catalog
from coordinate.apps.places.models import Place
from coordinate.apps.users.models import User

class Flag(Catalog):
    places = models.ManyToManyField(Place)


    class Meta(Catalog.Meta):
        verbose_name = "Flag"
        verbose_name_plural = "Flags"
