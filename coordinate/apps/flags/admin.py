# -*- coding: utf-8 -*-
from django.contrib import admin
from coordinate.apps.base.admin import CatalogAdminMixin
from coordinate.apps.flags.models import Flag
from coordinate.apps.flags.resources import FlagResource




@admin.register(Flag)
class FlagAdmin(CatalogAdminMixin, admin.ModelAdmin):
    resource_class = FlagResource
