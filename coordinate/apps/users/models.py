# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import
from django.core.validators import RegexValidator
from django.contrib.auth.models import AbstractUser
from django.db import models
from geoposition.fields import GeopositionField


class User(AbstractUser):
    PHONE_VALIDATOR = RegexValidator(
        regex=r'^\+?1?\d{10,13}$', message='Escribe correctamente el teléfono')
    mobile_phone = models.CharField(max_length=13, validators=[PHONE_VALIDATOR])
    address = models.CharField(max_length=255, blank=True)
    address_position = GeopositionField(blank=True)

    # home_url = models.CharField(max_length=64, choices=HOME_URL, default='/dashboard/semaforos')
