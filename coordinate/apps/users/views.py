# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# from django.shortcuts import render
from rest_framework import viewsets, status
from rest_framework.decorators import list_route
from rest_framework.permissions import IsAdminUser, AllowAny
from rest_framework.response import Response
from coordinate.apps.users.models import User
from coordinate.apps.users.serializer import UserSerializer


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Places to be viewed or edited.
    """
    model = User
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [IsAdminUser]
    lookup_field = 'username'

    @list_route(methods=['POST'], permission_classes=[AllowAny])
    def register(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
