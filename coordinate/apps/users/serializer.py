# -*- coding: utf-8 -*-
from mock import patch
from django.contrib.auth.password_validation import validate_password
from rest_framework import serializers
from coordinate.apps.users.models import User


class SimpleUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'last_name')


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(
        style={'input_type': 'password'}, write_only=True,
    )

    class Meta:
        model = User
        fields = '__all__'
        read_only_fields = (
            'last_login', 'date_joined', 'is_staff', 'is_superuser', 'is_active',
            'groups', 'user_permissions')
        lookup_field = 'username'
        extra_kwargs = {
            'url': {'lookup_field': 'username'}
        }

    def validate_password(self, value):
        validate_password(value)
        return value

    def create(self, *args, **kwargs):
        with patch('django.contrib.auth.models.UserManager.create', User.objects.create_user):
            return super(UserSerializer, self).create(*args, **kwargs)
