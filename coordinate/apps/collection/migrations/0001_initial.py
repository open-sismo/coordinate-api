# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-09-24 22:16
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import django.db.models.manager
import taggit.managers


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('taggit', '0002_auto_20150616_2121'),
        ('places', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Collection',
            fields=[
                ('place_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='places.Place')),
                ('contact_phone', models.CharField(blank=True, max_length=14)),
                ('contact_name', models.CharField(blank=True, max_length=128)),
                ('contact_email', models.EmailField(blank=True, max_length=254)),
                ('facebook', models.CharField(blank=True, max_length=128)),
                ('twitter', models.CharField(blank=True, max_length=128)),
                ('about_schedule', models.TextField(blank=True)),
                ('about_collection', models.TextField(blank=True)),
                ('about_status', models.TextField(blank=True)),
                ('remote_id', models.CharField(blank=True, max_length=50)),
                ('requirements', taggit.managers.TaggableManager(blank=True, help_text='A comma-separated list of tags.', through='taggit.TaggedItem', to='taggit.Tag', verbose_name='Tags')),
            ],
            options={
                'ordering': ('order',),
                'abstract': False,
            },
            bases=('places.place',),
            managers=[
                ('all_objects', django.db.models.manager.Manager()),
            ],
        ),
    ]
