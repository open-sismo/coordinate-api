import django_filters
from taggit.models import Tag
from coordinate.apps.collection.models import Collection
from coordinate.apps.places.filters import PlaceFilter


class CollectionFilter(PlaceFilter):

    class Meta(PlaceFilter.Meta):
        model = Collection
        fields = {
            'requirements__name': ["in"]
        }
        fields.update(PlaceFilter.Meta.fields)


class RequirementFilter(django_filters.FilterSet):
    class Meta:
        model = Tag
        fields = {
            "name": ['exact', 'icontains'],
            "slug": ['exact', 'icontains'],
        }
