
from __future__ import print_function

import os
from sys import argv

import httplib2
from apiclient import discovery
from django.core.management.base import BaseCommand, CommandError
from oauth2client import client, tools
from oauth2client.file import Storage

from coordinate.apps.collection.forms import CollectionForm
from coordinate.apps.collection.models import Collection
from coordinate.apps.places.models import PlaceType

# If modifying these scopes, delete your previously saved credentials
# at ~/.credentials/sheets.googleapis.com-python-quickstart.json
SCOPES = 'https://www.googleapis.com/auth/spreadsheets.readonly'
CLIENT_SECRET_FILE = 'client_id.json'
APPLICATION_NAME = 'Coordinate API'

SPREADSHEET_ID = "1ijleBcHJH_3V2nbMeXTjH4hTDYsjcdodYvHqhTc8C8c"

SOURCES = {
    'CMMX': {
        'SPREADSHEET_ID': '1ijleBcHJH_3V2nbMeXTjH4hTDYsjcdodYvHqhTc8C8c',
        'RANGE_NAME': "'Centros de Acopio - Colaborativo'!A2:AC"
    }
}


class Command(BaseCommand):
    help = "Sync data from Mapeo Colaborativo"

    def add_arguments(self, parser):
        parser.add_argument('client_id', nargs=1, type=str)
        # add parent arguments and defaults
        parser._add_container_actions(tools.argparser)
        try:
            defaults = tools.argparser._defaults
        except AttributeError:
            pass
        else:
            parser._defaults.update(defaults)

    def get_credentials(self, client_id=CLIENT_SECRET_FILE, flags=None):
        """Gets valid user credentials from storage.

        If nothing has been stored, or if the stored credentials are invalid,
        the OAuth2 flow is completed to obtain the new credentials.

        Returns:
            Credentials, the obtained credential.
        """
        home_dir = os.path.expanduser('~')
        credential_dir = os.path.join(home_dir, '.credentials')
        if not os.path.exists(credential_dir):
            os.makedirs(credential_dir)
        credential_path = os.path.join(credential_dir,
                                       'sheets.googleapis.com-python-quickstart.json')

        store = Storage(credential_path)
        credentials = store.get()
        if not credentials or credentials.invalid:
            flow = client.flow_from_clientsecrets(client_id, SCOPES)
            flow.user_agent = APPLICATION_NAME
            credentials = tools.run_flow(flow, store, flags)
            self.stdout.write('Storing credentials to ' + credential_path)
        return credentials

    def handle(self, *args, **options):
        try:
            client_id = options['client_id'][0]
        except (KeyError, IndexError):
            raise CommandError("User credentials not provided.")
        if not os.path.exists(client_id):
            raise CommandError("User credentials not found.")
        parser = self.create_parser(argv[0], argv[1])
        flags = parser.parse_args(argv[2:])
        credentials = self.get_credentials(client_id, flags)
        http = credentials.authorize(httplib2.Http())
        discoveryUrl = ('https://sheets.googleapis.com/$discovery/rest?'
                        'version=v4')
        service = discovery.build('sheets', 'v4', http=http,
                                  discoveryServiceUrl=discoveryUrl)
        for id_prefix, data in SOURCES.items():
            spreadsheet_id = data.get('SPREADSHEET_ID')
            range_name = data.get("RANGE_NAME")
            result = service.spreadsheets().values().get(
                spreadsheetId=spreadsheet_id, range=range_name).execute()
            values = result.get('values', [])

            if not values:
                self.stdout.write('No data found.')
                return
            self.stdout.write('Sync: {}'.format(id_prefix))
            for row in values:
                self.sync_collection(row, id_prefix)

    def clean_phone(self, contact_phone):
        if contact_phone:
            contact_phone = contact_phone.replace(' ', '')
            contact_phone = contact_phone.split(',')[0]
        return contact_phone

    def sync_collection(self, row, id_prefix):
        try:
            place_type_name = row[2]
            if place_type_name:
                place_type, created = PlaceType.objects.get_or_create(name=place_type_name)
            else:
                place_type = None
            requirements = row[3].split(',')
            remote_id = "{}:{}".format(id_prefix, row[0])
            name = row[1]
            contact_phone = self.clean_phone(row[5])
            about_collection = u"Tels: {}\n{}".format(row[5], row[20])
        except IndexError as err:
            self.stderr.write("ROW ERROR {!r}".format(err))
            return
        try:
            about_status = row[27]
        except IndexError:
            about_status = ''

        data = {
            'remote_id': remote_id,
            'name': name,
            'contact_name': row[4],
            'contact_phone': contact_phone,
            'twitter': row[6],
            'facebook': row[7],
            'contact_email': row[8],
            'address': row[16],
            'position_0': row[17],
            'position_1': row[18],
            'place_type': place_type.pk if place_type else None,
            'about_schedule': row[19],
            'about_collection': about_collection,
            'about_status': about_status,
            'active': bool(row[22]),
        }
        self.stdout.write(u'== {}'.format(name))

        try:
            instance = Collection.all_objects.get(remote_id=remote_id)
        except Collection.DoesNotExist:
            instance = None
        form = CollectionForm(data=data, instance=instance)
        if not form.is_valid():
            self.stderr.write(u"ERRORS: {!r}".format(form._errors))
            return
        collection = form.save()

        if collection.requirements.count():
            collection.requirements.all().delete()
        for requirement in requirements:
            collection.requirements.add(requirement.strip().lower())
        if not instance:
            self.stdout.write(u'Created {}'.format(collection))
        else:
            self.stdout.write(u'Updated {}'.format(collection))
