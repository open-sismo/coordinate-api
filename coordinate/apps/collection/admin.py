# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from coordinate.apps.places.admin import PlaceAdmin
from coordinate.apps.collection.models import Collection
from coordinate.apps.collection.resources import CollectionResource


@admin.register(Collection)
class CollectionAdmin(PlaceAdmin):
    resource_class = CollectionResource
    list_display = ('name', 'place_type', 'address', 'contact_name', 'contact_phone', 'active')
    search_fields = ('name', 'contact_name', 'contact_name', 'contact_phone', 'about_collection')
