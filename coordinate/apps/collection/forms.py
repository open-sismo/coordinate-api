from django import forms

from coordinate.apps.collection.models import Collection


class CollectionForm(forms.ModelForm):
    class Meta:
        model = Collection
        exclude = ('slug', 'order')
