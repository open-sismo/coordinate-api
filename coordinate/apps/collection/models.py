# -*- coding: utf-8 -*-
# VIVA
# MEXICO
# 2017
from __future__ import unicode_literals

from django.db import models
from taggit.managers import TaggableManager

from coordinate.apps.places.models import Place


class Collection(Place):
    contact_phone = models.CharField(max_length=14, blank=True, null=True)
    contact_name = models.CharField(max_length=128, blank=True, null=True)
    contact_email = models.EmailField(blank=True, null=True)
    facebook = models.CharField(max_length=128, blank=True, null=True)
    twitter = models.CharField(max_length=128, blank=True, null=True)
    about_schedule = models.TextField(blank=True, null=True)
    about_collection = models.TextField(blank=True, null=True)
    about_status = models.TextField(blank=True, null=True)
    remote_id = models.CharField(max_length=50, blank=True, null=True)
    requirements = TaggableManager(blank=True)
