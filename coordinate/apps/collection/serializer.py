# -*- coding: utf-8 -*-
from rest_framework import serializers
from taggit.models import Tag
from coordinate.apps.collection.models import Collection
from coordinate.apps.places.serializers import PlaceSerializer


class CollectionSerializer(PlaceSerializer):
    requirements = serializers.ListField(
        child=serializers.CharField(),
        source="requirements.all"
    )

    class Meta:
        model = Collection
        fields = '__all__'
        depth = 1


class RequirementSerializer(serializers.ModelSerializer):
    collections = serializers.IntegerField(read_only=True)

    class Meta:
        model = Tag
        fields = '__all__'
        depth = 1
        lookup_field = 'slug'
        extra_kwargs = {
            'url': {'lookup_field': 'slug'}
        }
