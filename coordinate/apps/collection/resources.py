# -*- coding: utf-8 -*-
from coordinate.apps.places.resources import PlaceResource
from coordinate.apps.collection.models import Collection


class CollectionResource(PlaceResource):

    class Meta(PlaceResource.Meta):
        model = Collection

    def dehydrate_requirements(self, obj):
        try:
            return ",".join([unicode(t) for t in obj.requirements.all()])
        except:
            return ""

    def save_m2m(self, obj, data, using_transactions, dry_run):
        if not dry_run:
            try:
                requirements = data.get('requirements').split(',')
            except:
                raise
            obj.requirements.set(*requirements)
        data['requirements'] = ",".join([str(t.pk) for t in obj.requirements.all()])
        return super(CollectionResource, self).save_m2m(obj, data, using_transactions, dry_run)
