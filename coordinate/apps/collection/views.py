# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db.models import Count
from django.http import HttpResponseRedirect
from rest_framework import filters, viewsets
from taggit.models import Tag

from coordinate.apps.collection.filters import (CollectionFilter,
                                                RequirementFilter)
from coordinate.apps.collection.models import Collection
from coordinate.apps.collection.serializer import (CollectionSerializer,
                                                   RequirementSerializer)


class CollectionViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows collections to be viewed or edited.
    """
    queryset = Collection.objects.all()
    serializer_class = CollectionSerializer
    filter_class = CollectionFilter
    lookup_field = 'slug'


def redirect_on_profile(request):
    # return HttpResponseRedirect(request.user.profile.home_url)
    # PROVISIONAL
    return HttpResponseRedirect(request.user)


class RequirementViewSet(viewsets.ModelViewSet):
    queryset = Tag.objects.annotate(collections=Count('collection'))
    serializer_class = RequirementSerializer
    filter_class = RequirementFilter
    lookup_field = 'slug'
