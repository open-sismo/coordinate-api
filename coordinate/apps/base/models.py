# -*- coding: utf-8 -*-
import itertools

from django.conf import settings
from django.db import models
from django.utils.text import slugify
from coordinate.apps.base.managers import CatalogManager


class Auditable(models.Model):
    """
    Basic absract class that will add fields for tracking:
    * created_at DateTimeField
    * created_by ForeignKey
    * modified_at DateTimeField
    * modified_by ForeignKey
    """
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    created_by = models.ForeignKey(
            settings.AUTH_USER_MODEL,
            related_name="created_by_%(app_label)s_%(class)s_set",
            editable=False, null=True)

    modified_at = models.DateTimeField(auto_now=True, editable=False)
    modified_by = models.ForeignKey(
            settings.AUTH_USER_MODEL,
            related_name="modified_by_%(app_label)s_%(class)s_set",
            editable=False, null=True)

    class Meta:
        abstract = True


class Catalog(Auditable):
    name = models.CharField(verbose_name="name", max_length=128)
    slug = models.SlugField(verbose_name="slug")
    active = models.BooleanField(verbose_name="active", default=True)
    order = models.PositiveIntegerField(verbose_name="order", default=0)

    all_objects = models.Manager()
    objects = CatalogManager()

    class Meta:
        abstract = True
        ordering = ("order",)

    def __unicode__(self):
        return self.name

    def _add_to_slug_filters(self, fields, filters):
        for field in fields:
            if field not in filters:
                filters[field] = getattr(self, field)

    def _save_slug_filters(self, potential):
        filters = {
            "slug": potential
        }
        unique_together = getattr(self._meta, "unique_together", [])
        if unique_together:
            first = unique_together[0]
            if isinstance(first, basestring) and "slug" in unique_together:
                self._add_to_slug_filters(unique_together, filters)
            elif isinstance(first, tuple) or isinstance(first, list):
                for fields in unique_together:
                    if "slug" in fields:
                        self._add_to_slug_filters(fields, filters)
        return filters

    def save(self, *args, **kwargs):
        model = self.__class__
        if not self.id:
            max_length = self._meta.get_field("slug").max_length

            potential = orig = slugify(self.name)[:max_length]

            for x in itertools.count(1):
                if not model.objects.filter(**self._save_slug_filters(potential)).exists():
                    self.slug = potential
                    break

                # Truncate the original slug dynamically.
                # Minus 1 for the hyphen.
                potential = "%s-%d" % (
                    orig[:max_length - len(str(x)) - 1], x
                )

        super(Catalog, self).save(*args, **kwargs)
