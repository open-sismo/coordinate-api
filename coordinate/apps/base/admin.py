from import_export.admin import ImportExportMixin
from reversion.admin import VersionAdmin


class CatalogAdminMixin(ImportExportMixin, VersionAdmin):
    change_list_template = "admin/base/catalog/change_list.html"
    list_display = ("name", "active",)
    prepopulated_fields = {"slug": ("name",)}
    list_editable = ("active",)
    list_filter = ('active',)
