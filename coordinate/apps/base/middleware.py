# -*- coding: utf-8 -*-
"""Core middleware."""
from django.contrib.auth.middleware import get_user
from django.db.models import signals
from django.utils.functional import SimpleLazyObject, curry
from coordinate.apps.base.models import Auditable


class AuditMiddleware(object):
    def __init__(self, get_response=None):
        self.get_response = get_response

    def __call__(self, request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.
        self.process_request(request)

        response = self.get_response(request)

        response = self.process_response(request, response)

        # Code to be executed for each request/response after
        # the view is called.

        return response

    def get_api_user(self, request):
        user = get_user(request)
        if user.is_authenticated():
            return user
        return user

    def process_request(self, request):
        request.user = SimpleLazyObject(lambda: self.get_api_user(request))
        if request.method not in ('GET', 'HEAD', 'OPTIONS', 'TRACE'):
            if hasattr(request, 'user') and request.user.is_authenticated():
                user = request.user
            else:
                user = None

            update_pre_save_info = curry(
                self._update_pre_save_info, user)
            signals.pre_save.connect(
                update_pre_save_info, dispatch_uid=(self.__class__, request,),
                weak=False)

    def process_response(self, request, response):
        signals.pre_save.disconnect(dispatch_uid=(self.__class__, request,))
        return response

    def _update_pre_save_info(self, user, sender, instance, **kwargs):
        if issubclass(sender, Auditable):
            if instance.created_by is None:
                instance.created_by = user
            instance.modified_by = user
