# -*- coding: utf-8 -*-
import django_filters
from django.contrib.auth import get_user_model

User = get_user_model()


class CreatedByChoiceFilter(django_filters.ModelChoiceFilter):
    def get_filter_predicate(self, v):
        return {"created_by__username": v.username}


class ModifiedByChoiceFilter(django_filters.ModelChoiceFilter):
    def get_filter_predicate(self, v):
        return {"modified_by__username": v.username}


class CatalogFilter(django_filters.FilterSet):
    created_by = CreatedByChoiceFilter(
        queryset=User.objects.all(), to_field_name="username")
    modified_by = ModifiedByChoiceFilter(
        queryset=User.objects.all(), to_field_name="username")

    class Meta:
        fields = {
            "name": ['exact', 'icontains'],
            "slug": ['exact', 'icontains'],
            "created_at": ['exact', 'lt', 'lte', 'gt', 'gte'],
            "modified_at": ['exact', 'lt', 'lte', 'gt', 'gte'],
            "created_by": ['exact'],
            "modified_by": ["exact"]
        }
