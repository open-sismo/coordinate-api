from rest_framework import serializers

from coordinate.apps.users.serializer import SimpleUserSerializer


class CatalogSerializer(serializers.ModelSerializer):
    created_by = SimpleUserSerializer(read_only=True)
    modified_by = SimpleUserSerializer(read_only=True)

    class Meta:
        lookup_field = 'slug'
        extra_kwargs = {
            'url': {'lookup_field': 'slug'}
        }
