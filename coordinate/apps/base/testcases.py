from django.contrib.auth import get_user_model
from django.urls import reverse


class JWTAuthTestCaseMixin(object):
    def setUp(self):
        super(JWTAuthTestCaseMixin, self).setUp()
        self.password = "password"
        self.user = get_user_model().objects.create_user("bart", "elbarto@eatmy.sh", self.password)
        self.jwt_auth(self.user)

    def jwt_auth(self, user):
        response = self.client.post(
            reverse('jwt-auth'),
            data=dict(username=self.user.username, password=self.password))
        token = response.data.get('token')
        self.assertIsNotNone(token)
        self.client.credentials(HTTP_AUTHORIZATION="JWT {}".format(token))
        return token
