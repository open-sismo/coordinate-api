from django.db import models


class CatalogManager(models.Manager):
    def get_queryset(self, *args, **kwargs):
        queryset = super(CatalogManager, self).get_queryset(*args, **kwargs)
        return queryset.filter(active=True)
