# -*- coding: utf-8 -*-
"""
Overridable settings.
"""
import os


try:
    from .base import *  # NOQA
except ImportError:
    pass

try:
    from .envs.active import *  # NOQA
except:
    env = os.environ.get("COORDINATE_ENV", 'dev')

    try:
        m = __import__(
            'coordinate.settings.envs.{}'.format(env),
            globals=globals(), locals=locals(), fromlist="*")
        try:
            attrlist = m.__all__
        except AttributeError:
            attrlist = dir(m)
        for attr in [a for a in attrlist if '__' not in a]:
            globals()[attr] = getattr(m, attr)
    except ImportError:
        pass

try:
    from .local import *  # NOQA
except ImportError:
    pass
