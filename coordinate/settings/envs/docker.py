from .dev import *  # NOQA

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'coordinate',
        'USER': 'coordinate',
        'PASSWORD': 'password',
        'HOST': 'db',
        'PORT': '',
    }
}

CACHES = {
    'default': {
        'BACKEND': 'redis_cache.RedisCache',
        'LOCATION': 'cache:6379',
    },
}
