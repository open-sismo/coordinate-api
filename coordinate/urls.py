"""coordinate URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth import views as auth_views
from rest_framework import routers
from rest_framework_jwt.views import obtain_jwt_token

from coordinate.apps.collection import views as collection
from coordinate.apps.collection.views import redirect_on_profile
from coordinate.apps.places import views as places
from coordinate.apps.users import views as users
from coordinate.apps.flags import views as flags

router = routers.DefaultRouter()
router.register(r'places', places.PlaceViewSet, 'places')
router.register(r'placetypes', places.PlaceTypeViewSet, 'placetypes')
router.register(r'collections', collection.CollectionViewSet, 'collections')
router.register(r'requirements', collection.RequirementViewSet, 'requirements')
router.register(r'users', users.UserViewSet, 'users')
router.register(r'flags', flags.FlagViewSet, 'flags')

urlpatterns = [
    url(r'^api/v1/', include(router.urls)),
    url(r'^api/v1/jwt-auth/', obtain_jwt_token, name="jwt-auth"),
    url(r'^admin/', admin.site.urls),
    # url('^', include('django.contrib.auth.urls')),
    url(r'^login/$', auth_views.LoginView.as_view(template_name='coordinate/login.html')),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^accounts/profile/', redirect_on_profile),

]
