#!/bin/sh
pip install -r requirements/$COORDINATE_ENV.txt
python manage.py migrate --noinput
python manage.py collectstatic --noinput
