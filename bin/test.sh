cat "${CI_PROJECT_DIR}/coordinate/settings/envs/${COORDINATE_ENV}.py"
pip install -r requirements/ci.txt
coverage erase
coverage run manage.py test
coverage report -m
coverage html -d cover/
