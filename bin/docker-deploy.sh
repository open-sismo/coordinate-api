#!/bin/sh
COMPOSE=/usr/bin/docker-compose
$COMPOSE -f "$1" pull
$COMPOSE -f "$1" stop web
$COMPOSE -f "$1" start web
