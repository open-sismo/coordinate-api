Open Sismo - Coordinate API
===========================
API to register donation hotspots, claim them. request donations.


[![pipeline status](https://gitlab.com/open-sismo/coordinate-api/badges/master/pipeline.svg)](https://gitlab.com/open-sismo/coordinate-api/commits/master)
[![coverage report](https://gitlab.com/open-sismo/coordinate-api/badges/master/coverage.svg)](https://gitlab.com/open-sismo/coordinate-api/commits/master)

## Setup

```bash
# 1. Create a virtualenv using virtualenvwrapper
mkvirtualenv coordinate-api

# 2. Install requirements
pip install -r requirements/dev.txt
```

## Development

Run the server

```bash
python manage.py runserver
```

## Docker

### Requirements

* docker
* docker-composer

https://docs.docker.com/compose/install/

### Run containers

```bash
#  To run the web server
docker-compose up web
# To run just the db
docker-compose up db
# To run the tests
docker-compose run web /code/bin/tests.sh
```

To attatch to a container use the following command:
```bash
docker exec -i -t coordinateapi_web_1 /bin/bash
```

## Project Docs

https://gitlab.com/open-sismo/coordinate-api/wikis/home
