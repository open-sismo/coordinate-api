FROM python:2.7
ENV PYTHONUNBUFFERED 1
ENV TERM dumb
RUN mkdir -p /code/requirements
WORKDIR /code
ADD requirements/ /code/requirements/
RUN pip install -r requirements/docker.txt
RUN \
  apt-get update && \
  apt-get install -y wget ca-certificates lsb-release build-essential
RUN sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
RUN wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
RUN \
  apt-get update && \
  apt-get install -y postgresql-client-9.6
ADD . /code/
